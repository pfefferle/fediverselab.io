
---
layout: "post"
title: "Fedicrypt - what would it take?"
date: 2018-06-05
tags:
    - friendica
preview: "Hypolite Petovan, Friendica developer, wrote a summary of making private data in Fediverse macroblogging networks truely private. Join the discussion"
url: "https://blog.mrpetovan.com/web-development/encrypting-private-data-in-a-social-network-web-app"
lang: en
---

Fediverse social networks hugely benefit from federated architecture. However, like centralized platforms, they store all the data in the cloud. Fediverse trust model implies trust in one's server administrator and hosting provider. Latest [research by Leah](https://chaos.social/@leah/99837391793032137) shows that administrators tend to choose large, popular providers. Judging by network statistics, users tend to flock to large Fediverse instances.

Hypolite Petovan, Friendica developer, [wrote a summary](https://blog.mrpetovan.com/web-development/encrypting-private-data-in-a-social-network-web-app) of what it would take to make private data in Fediverse macroblogging networks truely private. He uses Friendica network as a reference, but this applies to all Fediverse networks that have direct ("private") messages.

If you consider this topic important, join the [discussion](https://friendica.mrpetovan.com/display/735a2029185b15e777d10b9559020973).
