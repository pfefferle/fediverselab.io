
---
layout: "post"
title: "diaspora* 0.7.0 major release"
date: 2017-08-20
tags:
    - diaspora
preview: "This is one of the biggest updates. The new federation protocol included as part of 0.7 is incompatible with versions older than 0.6.3.0."
url: "https://www.neowin.net/news/federated-social-network-diaspora-delivers-a-new-major-release-and-is-available-for-download"
lang: en
---

This is one of the biggest updates that the software has ever received with over 28,675 lines of added code and 20,019 lines being removed. The release comes with an important notice for podmins (admins of different Diaspora pods/nodes). The new federation protocol that has been included as part of 0.7 is incompatible with versions older than 0.6.3.0 and if you’re still running a version older than that, your server won’t be able to fully communicate with servers running the newest software.
More info [here](https://www.neowin.net/news/federated-social-network-diaspora-delivers-a-new-major-release-and-is-available-for-download).
