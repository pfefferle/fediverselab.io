
---
layout: "generic"
title: "PeerTube"
network: "peertube"
subtitle: "Take back the control of your videos"
banner: "/img/peertube-bg.jpg"
percent: "45% 15%"
---
