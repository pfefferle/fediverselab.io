
# CONTRIBUTING

Don't hesitate to send a merge request.

### [Submitting software](submitting-software)

Tools submitted to **MORE APPS** website page **must be**:
* Fully open source (please, consider showcasing your closed source apps on https://the-federation.info)
* Supporting or planning to support one of the following protocols: OStatus, diaspora, Zot, ActivityPub (please, consider showcasing your apps federating via other protocols on https://the-federation.info)

Fork this repository. Add your project data to `./source/_data/miscellaneous.json` file.

A project **must have** *title, source, protocols (1 string, comma separated), categories* and appropriate protocol classes marked as `true`.

Please, choose no more than **2 categories** (array of strings) for the tool. The ones it was initially designed for.

#### Categories (WIP, may change in the future)
* `SN-ma` (social network: macroblogging)
* `SN-mi` (social network: microblogging)
* `Blog-Pub` (blog and publishing software)
* `Media` (media sharing: images, audio, etc)
* `Links` (link sharing)
* `Ev-Meet` (events, meetups, calendars)
* `Files` (file hosting software)
* `Coop` (cooperatives, shared management)
* `Econ` (economic activities)
* `DevTools` (developer tools: libraries and such)
* `Plugins` (plugins)

A project may have a logo / image (45x45px), placed in `./source/img/misc` folder.

### [Submitting news](submitting-news)

**Data located in**: `/source/_posts` folder

**[Chronicles](https://fediverse.party/en/chronicles)** page aggregates latest news about major releases, development, interviews, related projects of Fediverse *social networks* (i.e., all the networks listed on main page).

Posts are available via [RSS](https://fediverse.party/atom.xml) subscription.
`Preview` is rendered on Chronicles page (limited ammount), text after metadata is shown in RSS (unlimited) and on post's page (see [Tags](https://fediverse.party/tags) ).

Every post **must have** the following __metadata__:

```
layout: "post"
title: "some title"
date: 2222-01-25
tags:
    - pleroma
preview:
  "short gist..."
url: "https://pleroma.social/link-to-news-source"
lang: en
```

`Tags`
A post may have one of these tags: fediverse, gnusocial, diaspora, friendica, hubzilla, mastodon, postactiv, pleroma, socialhome, ganggo, misskey, peertube, osada

`Preview`
Limit 150 characters - for "regular" and "wanted" posts, limit 350 characters - for "featured" post.
Please, stay within the limits in preview, otherwise it gets truncated half-sentence and will be posted that way on the Chronicles front page. Not good.

__Optional metadata__:

```
wanted: true
featured: true
banner: "pic.jpg"
authors: [{"name": "John Snow", "url": "https://ggg.social", "network": "socialhome"}]
```

`Wanted`
Add this metadata to a post that you wish to show in the upper visible part of the Chronicles page. Calls for contribution, donations, help should be posted with this metadata.

`Featured`
Add this metadata to a new internal website's article, to show the post in a prominent part of the Chronicles page.

`Wanted` and `featured` can't be mixed and are temporary. This metadata must be removed from an older post when creating a new "featured" or "wanted" post.

`Banner`
Required for "featured" posts only, an image wide enough to be used as a fullscreen background, should be placed in `/source/_posts/exact-post-file-name` folder. See [example](https://gitlab.com/fediverse/fediverse.gitlab.io/tree/master/source/_posts/fediverse-saves-from-pickup-artists-and-7-more-reasons-to-join).

`Authors`
Required for "featured" posts only. Add an array of object(s): name you wish to be shown as the author, and one website link. If it's the link to yout account on Fediverse, specify network name - lower case, without spaces.

### [Writing internal article](Writing-internal-article)

Each new internal article will be posted as "featured" in the prominent part of the Chronicles page, and will stay there for a while. It will also be distributed via RSS subscription. Your name will be featured as the author(s).

A merge request with an article will include all the things mentioned above for a `featured` post: a file placed in `/source/_posts` folder with necessary metadata, a 350-character preview and full text after metadata section, written in markdown. A banner image should be added to `/source/_posts/exact-post-file-name` folder that you'll create. Any other images used in the article may be placed there as well.

**Thanks!**
