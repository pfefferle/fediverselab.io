
const UglifyJsPlugin = require('../../node_modules/uglifyjs-webpack-plugin');

module.exports = {
  entry: {
        main: './source/assets/scripts/crystalball'
  },
  output: {
    filename: 'ball.min.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/
      },
    ],
  }
};
